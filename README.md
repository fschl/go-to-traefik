# go-to-traefik

A *very opinionated* quick way to generate *parts of* dynamic Traefik 2 configuration (for docker-compose files).
Start with some label snippets for docker-compose files.

Available at https://fschl.gitlab.io/go-to-traefik/ 

This is also my *first experiment* with Golang WebAssembly.

- <https://tutorialedge.net/golang/go-webassembly-tutorial/>
- <https://github.com/golang/go/wiki/WebAssembly>
- <https://docs.traefik.io/v2.1/routing/overview/>

## Build

`docker-compose build`

## Run locally

`docker-compose up` then visit http://localhost/

## ideas for future features

- generate documentation/README sections for port mappings/URLs for the service
- Q: is it possible to fill sections of OpenAPI config?
- generate traefik labels based on existing service declaration (port mappings)
- generate compose files for staging/CI environments
  - small templates for multi service compose files
  - generate local/dev compose
  - generate CI/staging env
  - generate production-like environment
- environment labels for common applications and application-combinations
  - e.g. mongo + mongo-express
- k8s ingress file???
