package main

import (
	"flag" // maybe move to cobra later https://github.com/spf13/cobra
	"fmt"
	"os"

	"gitlab.com/fschl/go-to-traefik/generate"
)

func printLicense() {
	fmt.Printf("2020 by fschl licensed under MIT\nSource: gitlab.com/fschl/go-to-traefik")
}

func printHelp() {
	fmt.Printf("go-to-treafik - generate docker-compose labels for traefik2 reverse proxy\n\n" +
		"Usage:\n" +
		"  go-to-traefik -subdomain <subdomain> -port <port> [flags]\n\n" +
		"supported flags:\n")

	flag.PrintDefaults()

	fmt.Printf("\n")
	printLicense()
	fmt.Printf("\n")
}

func generateSimpleAuth() {
	fmt.Println("Enter a Username") // We will use this to store the users input
	var username string             // Read the users input
	_, err := fmt.Scan(&username)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Enter a password") // We will use this to store the users input
	var pwd string                  // Read the users input
	_, err = fmt.Scan(&pwd)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Put this line inside dynamic.conf:")
	fmt.Println(generate.SimpleAuthDynamicConf(username, pwd))
}

func main() {
	var help, auth bool
	var reverse string

	labels := generate.TraefikDockerLabels{}

	flag.BoolVar(&help, "help", false, "Displays help text")
	flag.BoolVar(&auth, "auth", false, "generates simpleAuth from interactive input")
	flag.BoolVar(&labels.CI, "ci", false, "set true if you want to generate branch-name based staging URLs")
	flag.BoolVar(&labels.SimpleAuth, "simpleAuth", false, "set true for simpleAuth for Router")
	flag.BoolVar(&labels.SecurityHeaders, "securityHeaders", true, "set true for securityHeaders middleware for Router")
	flag.BoolVar(&labels.TLS, "tls", true, "set false if tls disabled")
	flag.StringVar(&reverse, "reverse", "test.domain.example.com", "a domain to reverse")
	flag.StringVar(&labels.Port, "port", "80", "port of the app running in docker")
	flag.StringVar(&labels.Path, "path", "", "Prefix for the path to add after ${DOMAIN}")
	flag.StringVar(&labels.Protocol, "protocol", "http", "supported protocols are http and tcp")
	flag.StringVar(&labels.Subdomain, "subdomain", "", "subdomain prepended to .${DOMAIN}")

	flag.Parse()

	if help {
		printHelp()
	} else if auth {
		generateSimpleAuth()
	} else if len(reverse) > 0 {
		fmt.Println(generate.ReverseDomainName(reverse))
	} else if len(labels.Subdomain) == 0 {
		printHelp()
		fmt.Fprintf(os.Stderr, "You must specify a subdomain\n")
	} else {
		fmt.Printf("generating for: \n\tsubomain: \"%s\" with path: \"%s\" and port %s\n\n", labels.Subdomain, labels.Path, labels.Port)
		var output string
		output = generate.LabelsFromStruct(labels)
		fmt.Printf("%s", output)
	}
}
