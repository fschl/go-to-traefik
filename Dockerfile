FROM golang:1.12-buster as build

WORKDIR /go/src/gitlab.com/fschl/go-to-traefik/

COPY go.mod ./
COPY ./generate/ ./generate/
COPY ./cliapp/ ./cliapp/
COPY ./wasm/ ./wasm/
RUN GOOS=js GOARCH=wasm go get -d -v ./...
RUN go test -v ./generate/
RUN GOOS=js GOARCH=wasm go build -o go-to-traefik.wasm wasm/main.go
# RUN GOOS=linux GOARCH=amd64 go build -o go-to-traefik_linux_amd64 cliapp/main.go
# RUN GOOS=darwin GOARCH=amd64 go build -o go-to-traefik_darwin_amd64 cliapp/main.go
# RUN GOOS=windows GOARCH=amd64 go build -o go-to-traefik_windows_amd64 cliapp/main.go

FROM nginx:mainline-alpine as prod
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY --from=build /go/src/gitlab.com/fschl/go-to-traefik/go-to-traefik.wasm /usr/share/nginx/html/
COPY --from=build /usr/local/go/misc/wasm/wasm_exec.js /usr/share/nginx/html/
COPY public/ /usr/share/nginx/html
RUN ls -lah /usr/share/nginx/html

EXPOSE 80
