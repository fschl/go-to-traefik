package generate

import (
	"strings"

	"golang.org/x/crypto/bcrypt"
)

func basicAuth(username, password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func traefikMiddlewarePrefix() string {
	return "    admin-auth:\n" +
		"      basicAuth:\n" +
		"        users:\n"
}

func traefikDockerAuthMiddleware(userhash, router string) string {
	return "      - \"traefik.http.middlewares." + router + "-auth.basicAuth.users=" + userhash + "\"\n" +
		"      - \"traefik.http.routers." + router + ".middlewares=" + router + "-auth\""
}

// SimpleAuthDynamicConf returns a string for traefik basicAuth configuration in dynamic.yml
func SimpleAuthDynamicConf(username, password string) string {
	if len(username) == 0 {
		return "Unable to generate Configuration because Username was empty!"
	}
	if len(password) == 0 {
		return "Unable to generate Configuration because Password was empty!"
	}
	passwordHash, _ := basicAuth(username, password)
	var parts []string
	parts = strings.SplitAfter(passwordHash, "$")
	hashForCompose := strings.Join(parts, "$")

	if checkPasswordHash(password, passwordHash) {
		authConfig := "        - \"" + username + ":" + passwordHash + "\"\n"
		result := "dynamic.conf for Traefik:\n"
		result += traefikMiddlewarePrefix() + authConfig + "\n"
		result += "Docker-Compose labels:\n"
		result += traefikDockerAuthMiddleware(username+":"+hashForCompose, "<yourRouterName>") + "\n"
		return result
	} else {
		return "Error while generating Username:Password configuration! :("
	}

}
