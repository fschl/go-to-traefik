package generate

import "testing"

func TestReverseDomainName(t *testing.T) {
	type args struct {
		url string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{"reverse order of TLD,domain,subdomain", args{"sub.domain.tld"}, "tld.domain.sub"},
		{"strip path if it was given", args{"sub.domain.tld/path"}, "tld.domain.sub"},
		{"strip strip https if it was given", args{"https://sub.domain.tld"}, "tld.domain.sub"},
		{"strip strip http if it was given", args{"http://sub.domain.tld"}, "tld.domain.sub"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ReverseDomainName(tt.args.url); got != tt.want {
				t.Errorf("ReverseDomainName() = %v, want %v", got, tt.want)
			}
		})
	}
}
