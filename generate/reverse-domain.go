package generate

import (
	"fmt"
	"strings"
)

// ReverseDomainName returns the reverse domain name notation of an URL
func ReverseDomainName(url string) string {
	components := strings.Split(url, ".")
	reverse := ""
	for key, value := range components {
		value = strings.TrimPrefix(value, "https://")
		value = strings.TrimPrefix(value, "http://")
		value = strings.Split(value, "/")[0]
		fmt.Printf("adding (%d) %s \n", key, value)
		reverse = value + "." + reverse
	}

	reverse = strings.TrimSuffix(reverse, ".")
	return reverse
}
