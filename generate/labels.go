package generate

import (
	"strings"
)

// TraefikDockerLabels supplies minimal struct for labels to generate
type TraefikDockerLabels struct {
	Protocol        string
	Subdomain       string
	Domain          string
	Path            string
	Port            string
	TLS             bool
	CI              bool
	SimpleAuth      bool
	SecurityHeaders bool
}

// LabelsFromStruct generates labels from TraefikDockerLabels struct
func LabelsFromStruct(labelConfig TraefikDockerLabels) string {
	return Labels(labelConfig.Protocol, labelConfig.Subdomain, labelConfig.Domain, labelConfig.Path, labelConfig.Port, labelConfig.TLS, labelConfig.CI, labelConfig.SimpleAuth, labelConfig.SecurityHeaders)
}

func generateRouterName(subdomain, path string, ci bool) string {
	var name string

	if len(subdomain) > 0 {
		name += subdomain
	}
	if len(path) > 0 {
		if len(subdomain) > 0 {
			name += "-"
		}
		name += path
	}
	if ci {
		name += "-${CI_COMMIT_REF_SLUG}"
	}
	name = strings.ReplaceAll(name, "@", "-")
	name = strings.ReplaceAll(name, "/", "-")
	name = strings.ReplaceAll(name, ".", "-")

	return name
}

func generateHostRule(subdomain, domain string) string {
	hostRule := "Host(`"
	if len(subdomain) > 0 {
		hostRule += subdomain + "."
	}
	if len(domain) > 0 {
		hostRule += domain
	} else {
		hostRule += "${DOMAIN}"
	}
	hostRule += "`)"

	return hostRule
}

func generatePathPrefix(path string, ci bool) string {
	pathPrefix := " && PathPrefix(`"
	if ci {
		pathPrefix += "/${CI_COMMIT_REF_SLUG}"
	}
	if len(path) > 0 {
		pathPrefix += "/" + path
	}
	pathPrefix += "`)"

	return pathPrefix
}

// Labels generates a bunch of service labels
func Labels(protocol, subdomain, domain, path, port string, tlsEnabled, ci, addSimpleAuth, addSecurityHeaders bool) string {
	if len(subdomain) == 0 && len(path) == 0 {
		return "Error: Subdomain and path cannot both be empty at the same time!\n"
	}

	path = strings.TrimPrefix(path, "/")
	path = strings.TrimSuffix(path, "/")

	name := generateRouterName(subdomain, path, ci)

	var returnvalue string
	const indent = "      "
	var traefikPrefix = "- \"traefik." + protocol + "."
	var routersPrefix = indent + traefikPrefix + "routers." + name
	const endline = "\"\n"

	if len(domain) > 0 {
		domainCleanReversed := ReverseDomainName(domain)
		returnvalue += indent + "- \"" + domainCleanReversed + "=" + name + endline
	}
	returnvalue += indent + "- \"traefik.enable=true" + endline

	returnvalue += routersPrefix + ".rule=" + generateHostRule(subdomain, domain)

	if len(path) > 0 || ci {
		returnvalue += generatePathPrefix(path, ci)
	}
	returnvalue += endline

	returnvalue += routersPrefix + ".service=" + name + endline
	if len(port) == 0 {
		port = "80"
	}
	returnvalue += indent + traefikPrefix + "services." + name + ".loadBalancer.server.port=" + port + endline

	if tlsEnabled {
		returnvalue += routersPrefix + ".tls=true" + endline
		returnvalue += routersPrefix + ".tls.certresolver=letsencrypt" + endline
		returnvalue += routersPrefix + ".entrypoints=web-secure" + endline
	} else {
		returnvalue += routersPrefix + ".entrypoints=web" + endline
	}

	middlewares := ""
	if addSecurityHeaders {
		middlewares += "security-headers@file"
	}
	if addSimpleAuth {
		if addSecurityHeaders {
			middlewares += ","
		}
		middlewares += "admin-auth@file"
	}
	if len(path) > 0 || ci {
		if addSecurityHeaders || addSimpleAuth {
			middlewares += ","
		}
		middlewares += name + "-stripprefix"
		returnvalue += indent + traefikPrefix + "middlewares." + name + "-stripprefix.stripprefix.prefixes=/"
		if ci {
			returnvalue += "${CI_COMMIT_REF_SLUG}/"
		}
		returnvalue += path + endline
	}
	returnvalue += routersPrefix + ".middlewares=" + middlewares + endline

	return returnvalue + "\n\n"
}
