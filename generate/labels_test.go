package generate

import (
	"strings"
	"testing"
)

func TestLabels(t *testing.T) {
	type args struct {
		protocol        string
		subdomain       string
		domain          string
		path            string
		port            string
		tlsEnabled      bool
		ci              bool
		addSimpleAuth   bool
		securityHeaders bool
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"show error if subdomain and pathPrefix are both empty", args{"http", "", "", "", "81", true, false, false, true}, "Error:"},
		{"traefik is enabled", args{"http", "sub", "domain", "path", "81", true, false, false, true}, "- \"traefik.enable=true\""},
		{"traefik listens on given port", args{"http", "sub", "domain", "path", "81", true, false, false, true}, "loadBalancer.server.port=81\""},
		{"default to port 80 if given port is empty", args{"http", "sub", "domain", "path", "", true, false, false, true}, "loadBalancer.server.port=80\""},
		{"router name is combination of subdomain and pathprefix", args{"http", "sub", "domain", "path", "81", true, false, false, true}, "routers.sub-path"},
		{"service loadBalancer must be set", args{"http", "sub", "domain", "path", "81", true, false, false, true}, "services.sub-path.loadBalancer.server.port=81"},
		{"use entrypoint web-secure when tls on", args{"http", "sub", "domain", "path", "81", true, false, false, true}, "entrypoints=web-secure"},
		{"use entrypoint web when tls off", args{"http", "sub", "domain", "path", "81", false, false, false, true}, "entrypoints=web"},
		{"add CI_COMMIT_REF_SLUG to path when CI is enabled", args{"http", "sub", "domain", "path", "81", true, true, false, true}, "PathPrefix(`/${CI_COMMIT_REF_SLUG}/"},
		{"also when path is empty", args{"http", "sub", "domain", "", "81", true, true, false, true}, "PathPrefix(`/${CI_COMMIT_REF_SLUG}"},
		{"and when CI is given but path is empty", args{"http", "sub", "domain", "", "81", true, true, false, false}, "http.routers.sub-${CI_COMMIT_REF_SLUG}.middlewares"},
		{"add middleware when CI is given but path is empty", args{"http", "sub", "domain", "", "81", true, true, false, false}, "http.routers.sub-${CI_COMMIT_REF_SLUG}.middlewares"},
		{"correctly set stripPrefixes when CI is given but path is empty", args{"http", "sub", "domain", "", "81", true, true, false, false}, "stripprefix.prefixes=/${CI_COMMIT_REF_SLUG}/"},
		{"add CI_COMMIT_REF_SLUG to router when CI is enabled", args{"http", "sub", "domain", "path", "81", true, true, false, true}, "routers.sub-path-${CI_COMMIT_REF_SLUG}"},
		{"add middleware when path is given", args{"http", "sub", "domain", "path", "81", true, false, true, true}, "http.routers.sub-path.middlewares"},
		{"should always contain security-headers@file", args{"http", "sub", "domain", "path", "81", true, false, true, true}, "middlewares=security-headers@file"},
		{"add \"admin-auth\" middleware if simpleAuth is enabled", args{"http", "sub", "domain", "path", "81", true, false, true, true}, "middlewares=security-headers@file,admin-auth"},
		{"add stripPathPrefix middleware when path is given", args{"http", "sub", "domain", "path", "81", true, false, true, true}, "http.middlewares.sub-path-stripprefix.stripprefix.prefixes"},
		{"prevent router starting with dash if subdomain is empty", args{"http", "", "domain", "path", "81", true, false, true, true}, "routers.path.rule="},
		{"prevent router starting with dash if subdomain is empty and CI is enabled", args{"http", "", "domain", "path", "81", true, true, false, true}, "routers.path-${CI_COMMIT_REF_SLUG}.rule="},
		{"omit dot before ${DOMAIN} if subdomain is empty", args{"http", "", "", "path", "81", true, true, false, true}, "Host(`${DOMAIN}"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Labels(tt.args.protocol, tt.args.subdomain, tt.args.domain, tt.args.path, tt.args.port, tt.args.tlsEnabled, tt.args.ci, tt.args.addSimpleAuth, tt.args.securityHeaders); !strings.Contains(got, tt.want) {
				t.Errorf("Labels() = %v, want %v", got, tt.want)
			} else {
				t.Logf("Passed: %s", tt.name)
			}
		})
	}
}
