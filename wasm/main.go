package main

import (
	"fmt"
	"syscall/js"

	"gitlab.com/fschl/go-to-traefik/generate"
)

func jsDocumentGetStringValue(htmlElement js.Value) string {
	return js.Global().Get("document").Call("getElementById", htmlElement.String()).Get("value").String()
}

func jsDocumentGetCheckboxValue(htmlElement js.Value) bool {
	return js.Global().Get("document").Call("getElementById", htmlElement.String()).Get("checked").Bool()
}

/*
runWasmFunc maps the HTML document elements provided by the JavaScript function to the golang struct.

some mix of:
- https://tutorialedge.net/golang/go-webassembly-tutorial/
- https://www.aaron-powell.com/posts/2019-02-06-golang-wasm-3-interacting-with-js-from-go/
- https://stackoverflow.com/questions/52972534/how-can-i-access-dom-element-properties-from-go-webassembly
*/
func runWasmFunc(this js.Value, args []js.Value) interface{} {
	fmt.Println("running WasmFunc")

	labels := generate.TraefikDockerLabels{}
	labels.Protocol = "http"
	labels.Subdomain = jsDocumentGetStringValue(args[0])
	labels.Domain = jsDocumentGetStringValue(args[1])
	labels.Path = jsDocumentGetStringValue(args[2])
	labels.Port = jsDocumentGetStringValue(args[3])
	labels.TLS = jsDocumentGetCheckboxValue(args[4])
	labels.CI = jsDocumentGetCheckboxValue(args[5])
	labels.SimpleAuth = jsDocumentGetCheckboxValue(args[6])
	labels.SecurityHeaders = jsDocumentGetCheckboxValue(args[7])

	fmt.Printf("Struct from JavaScript: \n%+v\n", labels)

	outputDiv := js.Global().Get("document").Call("getElementById", "output")
	output := generate.LabelsFromStruct(labels)
	// fmt.Printf("output: %s", output)
	outputDiv.Set("innerHTML", output)
	return nil
}

func createSimpleAuth(this js.Value, args []js.Value) interface{} {
	username := jsDocumentGetStringValue(args[0])
	password := jsDocumentGetStringValue(args[1])

	outputDiv := js.Global().Get("document").Call("getElementById", "simpleAuthOutput")
	output := generate.SimpleAuthDynamicConf(username, password)
	// fmt.Printf("output: %s", output)
	outputDiv.Set("innerHTML", output)
	return nil
}

func registerCallbacks() {
	js.Global().Set("runWasmFunc", js.FuncOf(runWasmFunc))
	js.Global().Set("createSimpleAuth", js.FuncOf(createSimpleAuth))
}

func main() {
	c := make(chan struct{}, 0)
	fmt.Println("initialized WASM")
	registerCallbacks()
	<-c
}
